from django.contrib import admin
from .models import Pertanyaan, Jawaban
# Register your models here.
admin.site.register(Pertanyaan)
admin.site.register(Jawaban)