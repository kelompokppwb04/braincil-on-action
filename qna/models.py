from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone

# Create your models here.

class Pertanyaan(models.Model):
    usernanya = models.ForeignKey(User, on_delete=models.CASCADE, default=0)
    matpel = models.CharField(default=0, max_length=100, choices = [
            ('Matematika', 'Matematika'),
            ('Fisika', 'Fisika'),
            ('Kimia', 'Kimia'),
            ('Biologi', 'Biologi'),
            ('IPS', 'IPS'),
            ('PKN', 'PKN'),
            ('Indonesia', 'Indonesia'), 
            ('Inggris', 'Inggris'),
            ('IT', 'IT'),
        ],)
    waktu = models.DateTimeField(default=timezone.now)
    pertanyaan = models.CharField(max_length=255)

    def __str__(self):
        return self.pertanyaan
    
    

class Jawaban(models.Model):
    userjawab = models.ForeignKey(User, on_delete=models.CASCADE, default=0)
    pertanyaan = models.ForeignKey(Pertanyaan, on_delete=models.CASCADE, default=0)
    waktu = models.DateTimeField(default=timezone.now)
    jawaban = models.CharField(max_length=255)
        
    def __str__(self):
        return self.jawaban