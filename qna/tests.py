
from django.test import LiveServerTestCase, TestCase, tag, Client, RequestFactory
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .views import question, hasil
from .forms import JawabanForm
from selenium import webdriver
from django.contrib.auth.models import User
from .models import Pertanyaan, Jawaban
from django.utils import timezone
from django.db import models
# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()
        
class QuestionUnitTestURL(TestCase):

    # Cek URL Register
    def test_apakah_ada_url_qna(self):
        response = Client().get('/qna/question/')
        self.assertEqual(response.status_code, 302)

    def test_apakah_view_dan_template_dari_path_kegiatan(self):
        response = self.client.get('/qna/question/')
        found = resolve('/qna/question/')
        self.assertEqual(found.view_name, "qna:question")
               
class QuestionUnitTestModel(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user2 = User.objects.create_user(username ="icuil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        self.pertanyaan = Pertanyaan.objects.create(
            usernanya=self.user,
            matpel="Matematika",
            pertanyaan="Apa kabar semua"
        )
        self.idpertanyaan = self.pertanyaan.id
        self.jawaban = Jawaban.objects.create(
            userjawab = self.user2,
            pertanyaan = self.pertanyaan,
            jawaban = "baik"
        )

    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek URL LOGIN dan LOGOUT
    def test_apakah_ada_url_pertanyaan(self):
        request = self.factory.get(reverse("qna:question"))
        request.user = self.user
        response = question(request)
        self.assertEquals(response.status_code, 200)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Matematika", html_kegiatan)
        
    #Cek Apakah Model Kegiatan Sudah Terbuat
    def test_apakah_model_pertanyaan_sudah_terbuat(self):
        jumlah_objek = Pertanyaan.objects.all().count()
        hasil_print = self.pertanyaan.__str__()
        self.assertEquals(hasil_print, "Apa kabar semua")
        self.assertEquals(jumlah_objek, 1)

    # Cek Apakah Model Kegiatan Sudah Terbuat
    def test_apakah_model_jawaban_sudah_terbuat(self):
        jumlah_objek = Jawaban.objects.all().count()
        hasil_print = self.jawaban.__str__()
        self.assertEquals(hasil_print, "baik")
        self.assertEquals(jumlah_objek, 1)

class JawabanUnitTestForm(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user2 = User.objects.create_user(username ="icuil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        self.pertanyaan = Pertanyaan.objects.create(
            usernanya=self.user,
            matpel="Matematika",
            pertanyaan="Apa kabar semua"
        )
        self.idpertanyaan = self.pertanyaan.id
        self.jawaban = Jawaban.objects.create(
            userjawab = self.user2,
            pertanyaan = self.pertanyaan,
            jawaban = "baik"
        )


    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek apakah form notpost valid
    def test_apakah_notpost_form_valid(self):
        form = JawabanForm()
        self.assertTrue(form.is_valid)
    
    # Cek apakah form post valid
    def test_apakah_post_form_valid(self):
        form = JawabanForm(data={
            'jawaban':"Alhamdulillah",
        })
        self.assertTrue(form.is_valid)
    
    # Cek apakah objek terbuat saat ada isian form
    def test_apakah_objek_dapat_dibuat_dari_form(self):
        data={
            'jawaban': "Alhamdulillah",
            'pertanyaan' : self.pertanyaan,
            'p_id' : self.idpertanyaan
        }
        
        request = self.factory.post(reverse("qna:question"), data=data)
        request.user = self.user
        response = question(request)
        jumlah = Jawaban.objects.all().count()
        self.assertGreaterEqual(jumlah, 1)
    
    def test_apakah_halaman_jawaban_benar(self):
        request = self.factory.get('/qna/hasil')
        request.user = self.user
        response = hasil(request, self.idpertanyaan)

        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("baik", html_kegiatan)

# Yang belum test view dan template