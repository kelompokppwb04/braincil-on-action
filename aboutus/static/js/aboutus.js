$(document).ready(function() {
    $("#accordion")
        .accordion({
            header: "> div > h3",
            collapsible: true
        });

    $(".up").click(function(e) {
        const current = $(this).parent().parent();
        current.insertBefore(current.prev())
        e.preventDefault();
        e.stopPropagation();
    });

    $(".down").click(function(e) {
        const current = $(this).parent().parent();
        current.insertAfter(current.next())
        e.preventDefault();
        e.stopPropagation();
    });
});