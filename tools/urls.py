from django.urls import path
from django.conf import settings
from django.conf.urls.static import static

from . import views

app_name = 'tools'

urlpatterns = [
    path('toactlist/', views.toactlist, name='toactlist'),
    path('gettask/', views.gettask, name='gettask'),
    path('createtask/', views.createtask, name='createtask'),
    path('deletetask/', views.deletetask, name='deletetask'),
    path('searchtask/', views.searchtask, name='searchtask'),
    path('tips/', views.tips, name='tips'),
    path('deletespecifictask/', views.deletespecifictask, name='deletespecifictask'),
]