from django import forms
from .models import ToActList

class DateInput(forms.DateInput):
    input_type = 'date'

class ToActListForm(forms.ModelForm):
    class Meta:
        model = ToActList
        fields = ['deadline', 'title']

    error_messages = {
        'required' : 'Please Type',
    }

    deadline_attrs = {
        'type': 'date',
        'class' : 'form-control',
        'required' : True,
        'id' : 'deadline',
    }

    title_attrs = {
        'placeholder' : 'To-Act',
        'class' : 'form-control',
        'required' : True,
        'id' : 'title',
    }

    deadline = forms.DateField(widget=forms.DateInput(attrs=deadline_attrs))
    title = forms.CharField(widget=forms.TextInput(attrs=title_attrs))