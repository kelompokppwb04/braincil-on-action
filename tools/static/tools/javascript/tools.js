$(document).ready(function(){

    // Get Cookie
    function getCookie(name) {
        let cookieValue = null;
        if (document.cookie && document.cookie !== '') {
            const cookies = document.cookie.split(';');
            for (let i = 0; i < cookies.length; i++) {
                const cookie = cookies[i].trim();
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) === (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    var csrftoken = getCookie('csrftoken');
    // Get Cookie

    // Fungsi append to-act list
    function appendToActList(request){
        console.log("Masuk sukses")
        var arr = request.items;
        
        $(".toactlistTable").empty();
        for (i=0; i<arr.length; i++){
            var deadline = arr[i].deadline;
            var title = arr[i].title;
            var id = arr[i].id;
            var number = i+1;

            var item = 

            '<tr>' +
                '<th scope="row">' + number + '</th>' +
                '<td id="deadline' + id + '" target="' + deadline + '">'  +  deadline + '</td>' +
                '<td id="title' + id + '" target="' + title + '">'  +  title + '</td>' +
                '<td> <button class="delete btn btn-primary mb-2" target=' + id + '> Delete </button> </td>' +
                '<td> <button class="edit btn btn-primary mb-2" target=' + id + '> Edit </button> </td>' +
            '</tr>'

            $(".toactlistTable").append(item);
        }

        $("#deadline").val("")
        $("#title").val("")

        // Delete Button 
        for (i=0; i<arr.length; i++){
            var deleteItem = $(".delete.btn.btn-primary.mb-2")[i]
            $(deleteItem).click(function(){
                $.ajax({
                    url : "/tools/deletespecifictask?q=" + $(this).attr('target'),
                    type : "GET",
                    headers: {
                        "X-CSRFToken": csrftoken,
                    },  
                    success : function(request){
                        appendToActList(request)
                    }
                });
            });
        }

        // Edit Button 
        for (i=0; i<arr.length; i++){
            var EditedItem = $(".edit.btn.btn-primary.mb-2")[i]
            $(EditedItem).click(function(){
                var deadlineItem = $("#deadline" + $(this).attr('target')).attr('target')
                var titleItem = $("#title"+ $(this).attr('target')).attr('target')
                $.ajax({
                    url : "/tools/deletespecifictask?q=" + $(this).attr('target'),
                    type : "GET",
                    headers: {
                        "X-CSRFToken": csrftoken,
                    },  
                    success : function(request){
                        appendToActList(request)
                        $("#deadline").val(deadlineItem)
                        $("#title").val(titleItem)
                    }
                });
            });
        }

    }
    // Fungsi append to-act list

    // Tampilkan list ketika dokumen baru dibuka
    console.log("Ready")
    $.ajax({
        url : "/tools/gettask/",
        type : "GET",
        headers: {
            "X-CSRFToken": csrftoken,
        },  
        success : function(request){
            appendToActList(request)
        }
    });
    // Tampilkan list ketika dokumen baru dibuka
    
    // Event click button submit
    $("#submit").click(function(){
        console.log("submitted");

        $.ajax({
            url : "/tools/createtask/",
            type : "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            data : {
                'deadline' : $("#deadline").val(),
                'title' : $("#title").val()
            },
            success : function(request){
                appendToActList(request)
            }
        });

    })
    // Event click button submit

    // Event click delete all   
    $("#deleteall").click(function(){
                
        console.log("masuk event delete")
        
        $.ajax({
            url : "/tools/deletetask/",
            type : "POST",
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            success : function(request){
                appendToActList(request)
            }
        });
        
    });
    // Event click delete all   

   
    // Event keyup search
    $("#search").keyup(function(){
        
        console.log($("#search").val())
        
        $.ajax({
            url : "/tools/searchtask/",
            type : "POST",
            data : {
                'val' :  $("#search").val(),
            },
            headers: {
                "X-CSRFToken": csrftoken,
            },  
            success : function(request){
                appendToActList(request)
            }
        });
    });
    // Event keyup search


    // Modal Bootstrap
    $('#exampleModalCenter').on('shown.bs.modal', function () {
        $('#myInput').trigger('focus')
    });
    // 

    // Event mouse up and mouse down untuk card


    // 

    // Event mouseover tombol tips
    $(".card.shadow.p-3.mb-5.bg-white.rounded").hide()
    $("#card1").show()
    

    $(".tips.btn.btn-primary.mb-2").mouseover(function(){
        $(".card.shadow.p-3.mb-5.bg-white.rounded").hide()
        $("#card"+$(this).attr('target')).fadeIn(1000)
        $(".tips.btn.btn-primary.mb-2.active").removeClass("active")
        $(this).addClass("active")
    });
    // Event hover tombol tips
});