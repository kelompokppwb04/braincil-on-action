from django.test import TestCase

# Create your tests here.
from django.test import TestCase, RequestFactory
from django.test import LiveServerTestCase, TestCase, tag, Client
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from django.contrib.auth.models import User
from .models import ToActList
from django.utils import timezone
from .views import toactlist, gettask, createtask, deletetask, searchtask, tips, deletespecifictask
from .forms import ToActListForm
from account.views import register, loginUser, logoutUser
import json
from django.contrib.auth.models import User

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

# TEST REGISTER   
class ToactlistUnitTest(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username = "ichadevii",
            password="123456789PPW",
        )
        self.toactlist = ToActList.objects.create(
            user = self.user,
            title = "Ngerjain TK2",
        )

    # Cek URL Register
    def test_apakah_ada_url_toactlist(self):
        response = Client().get('/tools/toactlist/')
        self.assertEqual(response.status_code, 200)
    
    #Cek model
    def test_apakah_model_toactlist_terbuat(self):
        jumlah_objek = ToActList.objects.all().count()
        self.assertEqual(jumlah_objek, 1)
        hasil = self.toactlist.__str__()
        self.assertEqual(hasil, "ichadevii")

    # Cek views dan template yang dipakai
    def test_apakah_view_dan_template_dari_path_toactlist(self):
        response = self.client.get('/tools/toactlist/')
        found = resolve('/tools/toactlist/')
        self.assertEqual(found.view_name, "tools:toactlist")
        self.assertTemplateUsed(response, 'tools/toactlist.html')
    
    # Cek form 
    def test_apakah_form_dapat_dibuat(self):
        form = ToActListForm(data={
            'title' : 'Ngerjain TK2',
        })
        self.assertTrue(form.is_valid)
        
class GettaskTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        self.objek = ToActList.objects.create(user=self.user, title="Ngerjain TK")
        self.id_objek = self.objek.id

    def tearDown(self):
        self.user_logout = Client().logout()
        ToActList.objects.all().delete()

    # Cek url gettasks
    def test_apakah_ada_url_gettask(self):
        request = self.factory.get(reverse("tools:gettask"))
        request.user = self.user
        response = gettask(request)
        self.assertEquals(response.status_code, 200)
    
    # Cek view gettask
    def test_apakah_objek_ditampilkan_di_html(self):
        all_toactlist_user = ToActList.objects.filter(user=self.user)
        list_items = []
        for toactlist in all_toactlist_user:
            item = {
                "deadline" : toactlist.deadline,
                "title" : toactlist.title,
                "id" : toactlist.id,
            }
            list_items.append(item)

        request = self.factory.get(reverse("tools:gettask"))
        request.user = self.user
        hasil_test = {"items" : list_items}

        response = gettask(request)
        hasil_response = json.loads(response.content.decode())
        self.assertEqual(hasil_response, hasil_test)

class createTaskTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        ToActList.objects.create(user=self.user, title="Ngerjain TK")

    def tearDown(self):
        self.user_logout = Client().logout()
        ToActList.objects.all().delete()
        
    # Cek url createtask
    def test_apakah_ada_url_create(self):
        data = {
            "title" : "UAS",
        }

        request = self.factory.post(reverse("tools:createtask"), data=data)
        request.user = self.user
        response = createtask(request)
        self.assertEquals(response.status_code, 200)
    
    # Cek view createtask
    def test_apakah_objek_terbuat_dan_ditampilkan_di_html(self):
        all_toactlist_user = ToActList.objects.filter(user=self.user)
        list_items = []
        for toactlist in all_toactlist_user:
            item = {
                "deadline" : toactlist.deadline,
                "title" : toactlist.title,
                "id" : toactlist.id,
            }
            list_items.append(item)

        list_items.append({
            "deadline" : None,
            "title" : "UAS",
            "id" : 2,
        })
        
        data = {
            "title" : "UAS",
        }
        request = self.factory.post(reverse("tools:createtask"), data=data)
        request.user = self.user
        hasil_test = {"items" : list_items}

        response = createtask(request)
        hasil_response = json.loads(response.content.decode())
        self.assertEqual(hasil_test, hasil_response)

class deleteTaskTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        ToActList.objects.create(user=self.user, title="Ngerjain TK")
        
    def tearDown(self):
        self.user_logout = Client().logout()
        ToActList.objects.all().delete()

    # Cek url createtask
    def test_apakah_ada_url_deletetask(self):
        data={"test" : "testaja"}
        request = self.factory.post(reverse("tools:deletetask"), data=data)
        request.user = self.user
        response = deletetask(request)
        self.assertEquals(response.status_code, 200)
    
    # Cek view deletetask
    def test_apakah_objek_terbuat_dan_ditampilkan_di_html_deletetask(self):
        all_toactlist_user = ToActList.objects.filter(user=self.user)
        list_items = []

        data = {"test" : "testaja"}
        request = self.factory.post(reverse("tools:deletetask"), data=data)
        request.user = self.user
        hasil_test = {"items" : list_items}

        response = deletetask(request)
        hasil_response = json.loads(response.content.decode())
        self.assertEqual(hasil_response, hasil_test)

        
class searchTaskTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        ToActList.objects.create(user=self.user, title="Ngerjain TK")
        
    def tearDown(self):
        self.user_logout = Client().logout()
        ToActList.objects.all().delete()

    # Cek url searchtask
    def test_apakah_ada_url_searchtask(self):
        data={"val" : "ngerjain"}
        request = self.factory.post(reverse("tools:searchtask"), data=data)
        request.user = self.user
        response = searchtask(request)
        self.assertEquals(response.status_code, 200)
    
    # Cek view searchtask
    def test_apakah_objek_terbuat_dan_ditampilkan_di_html_searchtask(self):
        val = "ngerjain"
        
        all_toactlist_user = ToActList.objects.filter(user=self.user)
        list_items = []
        for toactlist in all_toactlist_user:
            if toactlist!=None :
                if val.lower() in toactlist.title.lower() :
                    item = {
                        "deadline" : toactlist.deadline,
                        "title" : toactlist.title,
                        "id" : toactlist.id,
                    }
                    list_items.append(item)
        
        data = {
            "val" : "ngerjain",
        }
        request = self.factory.post(reverse("tools:searchtask"), data=data)
        request.user = self.user
        hasil_test = {"items" : list_items}

        response = searchtask(request)
        hasil_response = json.loads(response.content.decode())
        self.assertEqual(hasil_test, hasil_response)

class tipsTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
    
    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek url tips
    def test_apakah_ada_url_tips(self):
        request = self.factory.get(reverse("tools:tips"))
        request.user = self.user
        response = tips(request)
        self.assertEquals(response.status_code, 200)

     # Cek templates tips
    def test_apakah_ada_url_tips(self):
        request = self.factory.get(reverse("tools:tips"))
        request.user = self.user
        response = tips(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("1", html_kegiatan)
        self.assertIn("Plan Your Goals Wisely", html_kegiatan)
        

class deleteSpecificTask(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        self.objek1 = ToActList.objects.create(user=self.user, title="Ngerjain TK")
        self.objek2 = ToActList.objects.create(user=self.user, title="Ngerjain TK2")
        self.id_objek2 = self.objek2.id

    
    def tearDown(self):
        self.user_logout = Client().logout()
        ToActList.objects.all().delete()

    # Cek url tips
    def test_apakah_ada_url_deletespecifictask(self):
        request = self.factory.get('/tools/deletespecifictask?q=' + str(1))
        request.user = self.user
        response = deletespecifictask(request)
        self.assertEquals(response.status_code, 200)

    # Cek view deletetask
    def test_apakah_objek_terbuat_dan_ditampilkan_di_html_deletespecifictask(self):
        idnya = self.id_objek2
        objek_toactlist = ToActList.objects.get(id=idnya)
        
        objek_toactlist.delete()

        all_toactlist_user = ToActList.objects.filter(user=self.user)
        list_items = []
        for toactlist in all_toactlist_user:
            item = {
                "deadline" : toactlist.deadline,
                "title" : toactlist.title,
                "id" : toactlist.id,
            }
            list_items.append(item)

        hasil_test = {"items" : list_items}
        
        self.objek2 = ToActList.objects.create(user=self.user, title="Ngerjain TK2")
        self.id_objek2 = self.objek2.id
        request = self.factory.get('/tools/deletespecifictask?q=' + str(self.id_objek2))
        request.user = self.user
        response = deletespecifictask(request)
        hasil_response = json.loads(response.content.decode())
        self.assertEqual(hasil_response, hasil_test)


