from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib import messages
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from .forms import ToActListForm
from .models import ToActList
# Create your views here.
response={}
def toactlist(request):
    form = ToActListForm()
    response['form'] = form
    return render(request, 'tools/toactlist.html', response)

@login_required
def gettask(request):
    all_toactlist_user = ToActList.objects.filter(user=request.user)
    list_items = []
    for toactlist in all_toactlist_user:
        item = {
            "deadline" : toactlist.deadline,
            "title" : toactlist.title,
            "id" : toactlist.id,
        }
        list_items.append(item)
    print("berhasil gettask")
    hasil = {"items" : list_items}
    return JsonResponse(hasil)

@login_required
def createtask(request):
    print("bukanpost")
    if request.method == "POST":
        # userid = request.POST.get("userid")
        # user = User.objects.get(id=userid)

        title = request.POST.get("title")
        deadline = request.POST.get("deadline")
        
        ToActList.objects.create(user=request.user, deadline=deadline, title=title)
        all_toactlist_user = ToActList.objects.filter(user=request.user)
        list_items = []
        for toactlist in all_toactlist_user:
            item = {
                "deadline" : toactlist.deadline,
                "title" : toactlist.title,
                "id" : toactlist.id,
            }
            list_items.append(item)
        return JsonResponse({"items" : list_items})

@login_required
def deletetask(request):
    if request.method == "POST":
        # userid = request.POST.get("userid")
        # user = User.objects.get(id=userid)
        all_toactlist_user = ToActList.objects.filter(user=request.user)
        
        list_items = []

        for toactlist in all_toactlist_user:
            toactlist.delete()

        print("berhasil delete task")
        print(id)
        return JsonResponse({"items" : list_items})

@login_required
def searchtask(request):
    print("bukanpost")
    if request.method == "POST":
        # userid = request.POST.get("userid")
        # user = User.objects.get(id=userid)

        title = request.POST.get("val")
        
        all_toactlist_user = ToActList.objects.filter(user=request.user)
        list_items = []
        for toactlist in all_toactlist_user:
            if toactlist!=None :
                if title.lower() in toactlist.title.lower():
                    item = {
                        "deadline" : toactlist.deadline,
                        "title" : toactlist.title,
                        "id" : toactlist.id,
                    }
                    list_items.append(item)
        print("berhasil create task")
        return JsonResponse({"items" : list_items})
    
@login_required
def tips(request):
    return render(request, 'tools/tips.html')

@login_required
def deletespecifictask(request):
    idnya = request.GET['q']
    objek_toactlist = ToActList.objects.get(id=idnya)
    
    objek_toactlist.delete()

    all_toactlist_user = ToActList.objects.filter(user=request.user)
    list_items = []
    for toactlist in all_toactlist_user:
        item = {
            "deadline" : toactlist.deadline,
            "title" : toactlist.title,
            "id" : toactlist.id,
        }
        list_items.append(item)

    print("berhasil delete specific task")
    print(id)
    return JsonResponse({"items" : list_items})