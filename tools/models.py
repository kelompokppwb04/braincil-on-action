from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.

class ToActList(models.Model):
    user = models.ForeignKey(User, max_length=255, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    deadline = models.DateField(null=True)

    def __str__(self):
        return (str)(self.user)
    