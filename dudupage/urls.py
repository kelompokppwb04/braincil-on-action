from django.urls import path

from .views import index

app_name = 'dudupage'

urlpatterns = [
    path('dudu/', index , name='index'),
]