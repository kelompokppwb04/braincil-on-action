from django.test import LiveServerTestCase, TestCase, tag, Client, RequestFactory
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .views import index
from .models import duduModel
from .forms import duduForm
from selenium import webdriver
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

class duduUnitTestURL(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )

    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek URL LOGIN dan LOGOUT
    def test_apakah_ada_url_dudu(self):
        request = self.factory.get(reverse("dudupage:index"))
        request.user = self.user
        response = index(request)
        self.assertEquals(response.status_code, 200)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("apakah kamu punya pesan untuk seseorang ?", html_kegiatan)

class duduUnitTestModel(TestCase):
    def test_create_dudu(self):
        self.dudu = duduModel.objects.create(dari = 'aku')
        idnya = self.dudu.id
        jumlah = duduModel.objects.all().count()
        self.assertGreaterEqual(jumlah, 1)
        hasil_print = self.dudu.__str__()
        self.assertEqual(hasil_print, str(idnya)+".")


class duduUnitTestForm(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )

    def tearDown(self):
        self.user_logout = Client().logout()
        
    def test_form_valid(self):
        form = duduForm()
        self.assertTrue(form.is_valid)
    
    def test_apakah_model_bisa_dibuat_dari_form(self):
        data = {
            "dari" : "aku",
            "untuk" : "kamu",
            "pesan" : "yay",
        }
        
        request = self.factory.post(reverse("dudupage:index"), data=data)
        request.user = self.user
        response = index(request)
        jumlah = duduModel.objects.all().count()
        self.assertGreaterEqual(jumlah, 1)
