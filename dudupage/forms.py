from django import forms

class duduForm(forms.Form):
    dari = forms.CharField(
        label='Dari',
        max_length=20,
        widget= forms.TextInput(
         attrs={
          'class':'form-control col-7',
          'placeholder':'anonymous',}
      )
    )

    untuk = forms.CharField(
        label='Untuk',
        max_length=20,
        widget= forms.TextInput(
         attrs={
          'class':'form-control col-7',
          'placeholder':'dia',}
      )
    )

    pesan = forms.CharField(
        label='Pesan',
        widget= forms.Textarea(
         attrs={
          'class':'form-control',
          'placeholder':'isi dengan pesan kamu',}
      )
    )


