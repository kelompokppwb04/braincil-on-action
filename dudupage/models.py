from django.db import models

# Create your models here.
class duduModel(models.Model):
    dari = models.CharField(max_length=20, default='anonymous')
    untuk = models.CharField(max_length=20, default='dia')
    pesan = models.TextField(default='isi pesan kamu di sini')

    def __str__(self):
            return "{}.".format(self.id)