from django.test import TestCase
# Create your tests here.
from django.test import LiveServerTestCase, TestCase, tag, Client, RequestFactory
from django.urls import reverse, resolve
from selenium import webdriver
from django.http import HttpRequest
from .views import register, profilesetting, loginUser
from django.contrib.auth.models import User
from .forms import MyUserCreationForm, ProfileSettingForm, ProfileSettingForm2, ProfilePictForm, LoginForm
from .models import Profile

# Create your tests here.

@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()

# TEST REGISTER   
class RegisterUnitTestURL(TestCase):
    def setUp(self):
        User.objects.create(
            username="icil",
            email="kelompokppwb04@gmail.com",
            first_name="Kelompok PPW",
            last_name="B04",
            password="123456789PPW",
        )

    # Cek URL Register
    def test_apakah_ada_url_register(self):
        response = Client().get('/account/register/')
        self.assertEqual(response.status_code, 200)

    def test_apakah_view_dan_template_dari_path_kegiatan(self):
        response = self.client.get('/account/register/')
        found = resolve('/account/register/')
        self.assertEqual(found.view_name, "account:register")
        self.assertTemplateUsed(response, 'account/register.html')
    
class RegisterUnitTestHTML(TestCase):
    #Cek Isi HTML Kegiatan yang ada di views kegiatan
    def test_isi_html_account_register(self):
        request = HttpRequest()
        response = register(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("New to Braincil?", html_kegiatan)
        self.assertIn("Join Today!", html_kegiatan)

class RegisterUnitTestModel(TestCase):
    def setUp(self):
        self.user = User.objects.create(
            username="icil",
            email="kelompokppwb04@gmail.com",
            first_name="Kelompok PPW",
            last_name="B04",
            password="123456789PPW",
        )

    #Cek Apakah Model Kegiatan Sudah Terbuat
    def test_apakah_model_user_sudah_terbuat(self):
        jumlah_objek = User.objects.all().count()
        self.assertEquals(jumlah_objek, 1)
        hasil = self.user.__str__()
        self.assertEqual(hasil, "icil")
    
    def test_apakah_saat_membuat_objek_tersimpan_dengan_benar(self):
        objek_yang_dibuat = User.objects.get(username="icil")
        self.assertEquals(objek_yang_dibuat.username, "icil")
        self.assertEquals(objek_yang_dibuat.email, "kelompokppwb04@gmail.com")
        self.assertEquals(objek_yang_dibuat.first_name, "Kelompok PPW")
        self.assertEquals(objek_yang_dibuat.last_name, "B04")
        self.assertEquals(objek_yang_dibuat.password, "123456789PPW")

class RegisterUnitTestForm(TestCase):
    # Cek apakah form post valid
    def test_apakah_post_form_valid_register(self):
        form = MyUserCreationForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        self.assertTrue(form.is_valid)

    # Cek apakah objek terbuat saat ada isian form
    def test_apakah_objek_dapat_dibuat_dari_form_register(self):
        form = MyUserCreationForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        form.save()
        jumlah_objek_dari_form = User.objects.all().count()
        self.assertEqual(jumlah_objek_dari_form, 1)
    
class RegisterUnitTestView(TestCase):
    # Cek apakah post form dapat dilakukan
    def test_apakah_post_form_dapat_dilakukan_register(self):
        data = {
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        }
        response = Client().post('/account/register/', data)
        html_kegiatan = response.content.decode('utf 8')
        self.assertEqual(response.status_code, 302)

    #Cek apakah objek terbuat dari view
    def test_membuat_akun_dari_view(self):
        data = {
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        }
        response = Client().post('/account/register/', data)
        html_kegiatan = response.content.decode('utf 8')
        jumlah_objek_dari_form = User.objects.all().count()
        self.assertEqual(jumlah_objek_dari_form, 1)
    
    # Cek message warning
    def test_apakah_keluar_message_warning_register(self):
        data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PP",
        }
        response = Client().post('/account/register/', data)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Please enter the same password", html_kegiatan)

#


# TEST PROFILESETTING
class ProfileUnitTestURL(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )

    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek URL ProfileSetting
    def test_apakah_ada_url_profile(self):
        request = self.factory.get(reverse("account:profilesetting"))
        request.user = self.user
        response = profilesetting(request)
        self.assertEquals(response.status_code, 200)
    
class ProfileUnitTestForm(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )

    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek apakah form post valid
    def test_apakah_post_form_profile_valid(self):
        form1 = ProfileSettingForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        form2 = ProfileSettingForm2(data={
            'title' : 'S.Kom.',
            'jobs' : 'mahasiswa',
            'contact' : '087871833168',
        })
        self.assertTrue(form1.is_valid)
        self.assertTrue(form2.is_valid)
    
    # Cek apakah objek terbuat saat ada isian form
    def test_apakah_change_profile_dapat_dibuat_dari_form(self):
        form = ProfileSettingForm(data={
            'username':"icil",
            'email' : "kelompokppwb04@gmail.com",
            'first_name' : "Kelompok PPW",
            'last_name' : "B04",
            'password1' : "123456789PPW",
            'password2' : "123456789PPW",
        })
        form.save()
        jumlah_objek_dari_form = User.objects.all().count()
        self.assertEqual(jumlah_objek_dari_form, 2)
     
class ProfileUnitTestView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )

    def tearDown(self):
        self.user_logout = Client().logout()
        # Cek apakah post form dapat dilakukan
    def test_apakah_post_form_profile_dapat_dilakukan(self):
        data = {
            'title' : 'S.Kom.',
            'jobs' : 'mahasiswa',
            'contact' : '087871833168',
        }
        response = Client().post('/account/profilesetting/', data)
        self.assertEqual(response.status_code, 302)
    
#
    
#TEST LOGIN
class LoginUnitTestURL(TestCase):
    # Cek URL Login
    def test_apakah_ada_url_login(self):
        response = Client().get('/account/loginUser/')
        self.assertEqual(response.status_code, 200)

    #Cek apakah template dan view yang digunakan sudah sesuai
    def test_apakah_view_dan_template_dari_path_loginUser(self):
        response = self.client.get('/account/loginUser/')
        found = resolve('/account/loginUser/')
        self.assertEqual(found.view_name, "account:loginUser")
        self.assertTemplateUsed(response, 'account/loginUser.html')

class LoginUnitTestHTML(TestCase):
    #Cek Isi HTML Kegiatan yang ada di views kegiatan 
    def test_isi_html_account_loginUser(self):
        request = HttpRequest()
        response = loginUser(request)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Sign in Here!!!", html_kegiatan)
        self.assertIn("Don't have any account?", html_kegiatan)
    
class LoginUnitTestForm(TestCase):
    # Cek apakah form post valid
    def test_apakah_post_form_valid_loginUser(self):
        form = LoginForm(data={
            'username':"icil",
            'password1' : "123456789PPW",
        })
        self.assertTrue(form.is_valid)

class LoginUnitTestView(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", first_name = "Kelompok PPW", last_name = "B04", password="123456789PPW")

    # Cek bisa login atau tidak
    def test_apakah_bisa_login_setelah_membuat_akun(self):
        data={
            'username':"icil",
            'password1' : "123456789PPW",
        }
        response = Client().post('/account/loginUser/', data)
        self.assertEqual(response.status_code, 302)
    
    #Cek message warning
    def test_apakah_muncul_message_warning_saat_salah_username(self):
        data={
            'username':"asjafuowq",
            'password1' : "123456789PPW",
        }
        response = Client().post('/account/loginUser/', data)
        self.assertContains(response, "Invalid username or password")

class LogoutUnitTestURL(TestCase):
    # Cek URL Logout
    def test_apakah_ada_url_logout(self):
        response = self.client.get('/account/logoutUser/')
        self.assertEqual(response.status_code, 302)

    