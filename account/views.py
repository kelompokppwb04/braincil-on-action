from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from .forms import MyUserCreationForm, ProfileSettingForm, ProfileSettingForm2, ProfilePictForm, LoginForm
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from .models import Profile

# Create your views here.
response={}
def register(request):
    form = MyUserCreationForm(request.POST)
    if request.method=='POST' :
        if form.is_valid():
            userz = form.save()

            name = form.cleaned_data.get("username")
            passw = form.cleaned_data.get("password1")
            user = authenticate(username=name, password=passw)
            
            instance = Profile(user=userz)
            instance.save()
            
            messages.success(request, "User with name " + name +" is built successfully")
            return redirect ("/account/register/")
        else:
            messages.warning(request, form.errors)
    form = MyUserCreationForm()
    response['form'] = form
    return render(request, 'account/register.html', response)

def loginUser(request):
    form = LoginForm(request.POST)
    if request.method=='POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password1')
        
            user = authenticate(username=username, password=password)
            print (user)
            if user : 
                if user.is_active:
                    login(request, user)
                    return redirect('/')
            else:
                messages.warning(request, "Invalid username or password")
    
    form = LoginForm()
    response['form'] = form
    return render(request, 'account/loginUser.html', response)

@login_required
def logoutUser(request):
    logout(request)
    return redirect ('/')

@login_required
def profilesetting(request):
    if request.method=='POST' :
        form1 = ProfileSettingForm(request.POST, instance=request.user)
        form2 = ProfileSettingForm2(request.POST, instance=request.user.profile)
        pictform = ProfilePictForm(request.POST, request.FILES, instance=request.user.profile)
        if form1.is_valid() and form2.is_valid:
            form1.save()
            form2.save()

        if pictform.is_valid():
            pictform.save()
        
        return redirect ("/account/profilesetting/")
    
    form1 = ProfileSettingForm(instance=request.user)
    form2 = ProfileSettingForm2(instance=request.user)
    pictform = ProfilePictForm(instance=request.user)
    response['form1'] = form1
    response['form2'] = form2
    response['pictform'] = pictform
    return render (request, 'account/profilesetting.html',response)