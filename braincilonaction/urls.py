from django.contrib import admin
from django.urls import include, path
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('main.urls')),
    path('', include('dudupage.urls'), name="dudupage"),
    path('account/', include(('account.urls'), 'account'), name="account"),
    path('aboutus/', include('aboutus.urls')),
    path('qna/', include(('qna.urls'), 'qna'), name="qna"),
    path('tools/', include(('tools.urls'), 'tools'), name="tools"),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
