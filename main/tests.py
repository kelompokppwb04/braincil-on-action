from django.test import LiveServerTestCase, TestCase, tag, RequestFactory, Client
from django.urls import reverse, resolve
from django.contrib.auth.models import User
from selenium import webdriver
from .views import home
from .forms import PertanyaanForm
from qna.models import Pertanyaan
from qna.views import question


@tag('functional')
class FunctionalTestCase(LiveServerTestCase):
    """Base class for functional test cases with selenium."""

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        # Change to another webdriver if desired (and update CI accordingly).
        options = webdriver.chrome.options.Options()
        # These options are needed for CI with Chromium.
        options.headless = True  # Disable GUI.
        options.add_argument('--no-sandbox')
        options.add_argument('--disable-dev-shm-usage')
        cls.selenium = webdriver.Chrome(options=options)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super().tearDownClass()


class MainTestCase(TestCase):
    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        # You can also use path names instead of explicit paths.
        response = self.client.get(reverse('main:home'))
        self.assertEqual(response.status_code, 200)

    def test_home_url_using_home_view(self):
        found = resolve('/')
        self.assertEqual(found.func, home)

    def test_home_url_using_main_template(self):
        response = self.client.get('/');
        self.assertTemplateUsed(response, 'main/home.html')

# class MainFunctionalTestCase(FunctionalTestCase):
#     def test_root_url_exists(self):
#         self.selenium.get(f'{self.live_server_url}/')
#         html = self.selenium.find_element_by_tag_name('html')
#         self.assertNotIn('not found', html.text.lower())
#         self.assertNotIn('error', html.text.lower())

class PertanyaanUnitTestForm(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username ="icil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user2 = User.objects.create_user(username ="icuil", email = "kelompokppwb04@gmail.com", password="123456789PPW")
        self.user_login = Client().login(username ="icil", password="123456789PPW" )
        # self.pertanyaan = Pertanyaan.objects.create(
        #     usernanya=self.user,
        #     matpel="Matematika",
        #     pertanyaan="Apa kabar semua"
        # )
        # self.idpertanyaan = self.pertanyaan.id

    def tearDown(self):
        self.user_logout = Client().logout()

    # Cek apakah form notpost valid
    def test_apakah_notpost_form_valid(self):
        form = PertanyaanForm()
        self.assertTrue(form.is_valid)
    
    # Cek apakah form post valid
    def test_apakah_post_form_valid(self):
        form = PertanyaanForm(data={
            'matpel':"Matematika",
            'pertanyaan':"Apa kabar semua",
        })
        self.assertTrue(form.is_valid)
    
    def test_apakah_model_pertanyaan_sudah_terbuat(self):
        data={
            'pertanyaan' : 'Apa kabar',
            'matpel' : 'Hidup',
        }
        
        request = self.factory.post(reverse("main:home"), data=data)
        request.user = self.user
        response = home(request)
        html_kegiatan = response.content.decode('utf-8')
        self.assertIn(" Braincil provides you with solutions ", html_kegiatan)
        Pertanyaan(usernanya=self.user, pertanyaan="Apa kabar semua", matpel="Hidup").save()
        jumlah = Pertanyaan.objects.all().count()
        self.assertGreaterEqual(jumlah, 1)
        
        request = self.factory.get(reverse("qna:question"))
        request.user = self.user
        response = question(request)
        self.assertEquals(response.status_code, 200)
        html_kegiatan = response.content.decode('utf 8')
        self.assertIn("Hidup", html_kegiatan)
