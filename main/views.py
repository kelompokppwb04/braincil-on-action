
from django.shortcuts import render, redirect
from .forms import PertanyaanForm
from qna.models import Jawaban, Pertanyaan
from django.contrib.auth.decorators import login_required

response = {}
def home(request):
    form = PertanyaanForm(request.POST)
    if request.method=='POST' :
        if form.is_valid():
            pertanyaans = request.POST.get('pertanyaan')
            matpels = request.POST.get('matpel')
            Pertanyaan(usernanya=request.user, pertanyaan=pertanyaans, matpel=matpels).save()
            return redirect('/qna/question/')
    form = PertanyaanForm()
    response['form'] = form
    return render(request, 'main/home.html', response)
